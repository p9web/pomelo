<?php

use Illuminate\Support\Facades\Route;


Route::get('/clinics', 'ClinicsController@find');
// Route::get('/clinic/{id}', 'ClinicsController@get');
// Route::post('/clinic/', 'ClinicsController@create');


Route::get('patients/{clinic_id}', 'PatientsController@findPatientsByClinic');
// Route::get('patients', 'PatientsController@find');
// Route::get('patient/{id}', 'PatientsController@get');
// Route::post('patient', 'PatientsController@create');


Route::get('providers/{clinic_id}', 'ProvidersController@findProvidersByClinic');
// Route::get('providers', 'ProvidersController@find');
// Route::get('provider/{id}', 'ProvidersController@get');
// Route::post('provider', 'ProvidersController@create');


Route::get('availabilities/{provider_id}/{from}/{to}', 'AvailabilitiesController@find');
// Route::get('availability/{provider_id}/{start_timestamp}', 'AvailabilitiesController@get');
// Route::post('availability', 'AvailabilitiesController@create');


Route::post('appointment', 'AppointmentsController@create');
// Route::get('appointment/{provider_id}/{start_timestamp}', 'AppointmentsController@get');
// Route::get('appointments/{provider_id}/{from}/{to}', 'AppointmentsController@find');
