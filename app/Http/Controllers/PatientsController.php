<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Clinic;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    /**
     * Function that returns all patients
     *
     * @return array
     */
    public function find()
    {
        return Patient::all();
    }


    /**
     * Function that returns a provider that matches an id
     *
     * @param $clinic_id string
     *
     * @return array
     */
    public function findPatientsByClinic(string $clinic_id)
    {
        request()->validate(['id' => ['string']]);

        return Patient::where('clinic_id', $clinic_id)->get();
    }


    /**
     * Function that returns a patient that matches an id
     *
     * @param $id string
     *
     * @return Patient | false
     */
    public function get(string $id)
    {
        request()->validate(['id' => ['string']]);

        $patient = Patient::orWhere(Patient::raw("first_name || last_name"), $id)
            ->first();

        if (!$patient)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        return $patient;
    }


    /**
     * Function that creates a new patient
     *
     * @param $request Request
     *
     * @return Patient | false
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
        ]);

        $clinic = Clinic::where('clinic_id', $request->clinic_id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        return Patient::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'clinic_id' => request('clinic_id'),
        ]);
    }
}
