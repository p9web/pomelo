<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Availability;
use App\Patient;
use App\Provider;

class AppointmentsController extends Controller
{
    // 15 minutes converted into timestamp integer
    private $time_slot_length = 60 * 60 * 15;

    /**
     * Useful function that rounds given timestamp to the closest quarter of hour
     *
     * @param integer $timestamp
     *
     * @return integer
     */
    private function round_timestamp(int $timestamp) {
        return round($timestamp / (15 * 60)) * (15 * 60);
    }

    /**
     * Returns a list of appointments for a specific provider in a specific timeframe
     *
     *
     * @param $provider_id string
     * @param integer $from
     * @param integer $to
     *
     * @return array | false
     */
    public function find(string $provider_id, int $from, int $to)
    {
        $now = time();
        if ($from < $now)
        {
            $from = $now;
        }

        if ($to < $now)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(401);
            return false;
        }

        request()->validate([
            'provider_id' => ['string'],
            'from' => ['integer'],
            'to' => ['integer', 'gt:from'],
        ]);

        // An appointment is an availability with no patient_id
        $appointments = Availability::where('provider_id', $provider_id)
            ->whereNotNull('patient_id')
            ->where('start_timestamp', '>=', $from)
            ->where('start_timestamp', '<', ($to + $this->time_slot_length))
            ->get();

        return ['appointments' => $appointments];
    }


    /**
     * Returns a provider's appointment at a specific timestamp
     *
     * @param $provider_id string
     * @param $start_timestamp integer
     *
     * @return Availability | null
     */
    public function get(string $provider_id, int $start_timestamp)
    {
        $start_timestamp = $this->round_timestamp($start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        // An appointment is nothing but an availability with no patient_id
        return Availability::where('provider_id', $provider_id)
            ->whereNotNull('patient_id')
            ->where('start_timestamp', $start_timestamp)
            ->first();
    }


    /**
     * Creates a new appointment for a specific provider in at specific timestamp with a specific patient
     *
     * @param $request Request
     *
     * @return Availability | false
     */
    public function create(Request $request)
    {
        $start_timestamp = $this->round_timestamp($request->start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'patient_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        $availability = Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->first();

        $provider = Provider::orWhere(Provider::raw("first_name || last_name"), $request->provider_id)
            ->first();

        $patient = Patient::orWhere(Patient::raw("first_name || last_name"), $request->patient_id)
            ->first();

        if (!$availability || !$patient || !$provider)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(401, 'Something went awfully wrong...');
            return false;
        }

        $patient_double_booking = Availability::where('patient_id', $request->patient_id)
            ->where('start_timestamp', $start_timestamp)
            ->first();


        if ($patient_double_booking)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(401, 'Patient already booked');
            return false;
        }

        Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->update([
                'patient_id' => $request->patient_id,
            ]);

        return Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->where('patient_id', $request->patient_id)
            ->get();
    }
}
