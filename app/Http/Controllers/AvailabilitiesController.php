<?php

namespace App\Http\Controllers;

use App\Availability;
use Illuminate\Http\Request;

class AvailabilitiesController extends Controller
{
    // 15 minutes converted into timestamp integer
    private $time_slot_length = 60 * 60 * 15;

    // Useful function that rounds given timestamp to the closest quarter of hour
    private function round_timestamp($timestamp) {
        return round($timestamp / (15 * 60)) * (15 * 60);
    }

    /**
     * Returns a list of availabilities for a specific provider in a specific timeframe
     *
     * @param $provider_id string
     * @param $from integer
     * @param $to integer
     *
     * @return array
     */
    public function find(string $provider_id, int $from, int $to)
    {
        // Nevermind the timestamps validation here...
        //        $now = time();
        //        if ($from < $now)
        //        {
        //            $from = $now;
        //        }
        //
        //        if ($to < $now)
        //        {
        //            abort(400);
        //            return [];
        //        }

        request()->validate([
            'provider_id' => ['string'],
            'from' => ['integer'],
            'to' => ['integer', 'gt:from'],
        ]);

        return Availability::where('provider_id', $provider_id)
            ->whereNull('patient_id')
            ->where('start_timestamp', '>=', $from)
            ->where('start_timestamp', '<', ($to + $this->time_slot_length))
            ->get();
    }


    /**
     * Returns a provider's availability at a specific timestamp
     *
     * @param $provider_id string
     * @param $start_timestamp integer
     *
     * @return Availability | null
     */
    public function get(string $provider_id, int $start_timestamp)
    {
        $start_timestamp = $this->round_timestamp($start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        // An appointment is nothing but an availability with no patient_id
        return Availability::where('provider_id', $provider_id)
            ->whereNull('patient_id')
            ->where('start_timestamp', $start_timestamp)
            ->first();
    }


    /**
     * Creates a new availability for a specific provider in at specific moment
     *
     * @param $request Request
     *
     * @return Availability | false
     */
    public function create(Request $request)
    {
        $start_timestamp = $this->round_timestamp($request->start_timestamp);

        $now = time();
        if ($start_timestamp < $now)
        {
            // TODO: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        return Availability::create([
            'provider_id' => $request->provider_id,
            'patient_id' => null,
            'start_timestamp' => $start_timestamp,
            'end_timestamp' => ($start_timestamp + $this->time_slot_length),
        ]);
    }
}
