<?php

namespace App\Http\Controllers;

use App\Provider;
use App\Clinic;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    /**
     * Function that returns all patients
     *
     * @return array
     */
    public function find()
    {
        return Provider::all();
    }


    /**
     * Function that returns a provider that matches an id
     *
     * @param $clinic_id string
     *
     * @return array
     */
    public function findProvidersByClinic(string $clinic_id)
    {
        request()->validate(['id' => ['string']]);

        return Provider::where('clinic_id', $clinic_id)->get();
    }


    /**
     * Function that returns a provider that matches an id
     *
     * @param $id string
     *
     * @return Provider | false
     */
    public function get(string $id)
    {
        request()->validate(['id' => ['string']]);

        $provider = Provider::orWhere(Provider::raw("first_name || last_name"), $id)
            ->first();

        if (!$provider)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        return $provider;
    }


    /**
     * Function that creates a new provider
     *
     * @param $request Request
     *
     * @return Provider | false
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
        ]);

        $clinic = Clinic::where('clinic_id', $request->clinic_id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        $payload = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'clinic_id' => $request->clinic_id,
        ];

        return Provider::create($payload);
    }
}
