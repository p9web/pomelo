<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clinic;

class ClinicsController extends Controller
{
    /**
     * Function that returns all clinics
     *
     * @return array
     */
    public function find()
    {
        return Clinic::all();
    }


    /**
     * Function that returns a clinic that matches an id
     *
     *@param $id string
     *
     * @return array | false
     *
     */
    public function get(string $id)
    {
        request()->validate([
            'clinic_id' => ['string'],
        ]);

        $clinic = Clinic::where('clinic_id', $id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return false;
        }

        return $clinic;
    }


    /**
     * Function that creates a new patient
     *
     * @param $request Request
     *
     * @return Clinic | false
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
        ]);

        return Clinic::create([
            'clinic_id' => $request->clinic_id,
        ]);
    }
}
