<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id',
        'patient_id',
        'start_timestamp',
        'end_timestamp',
    ];

}
