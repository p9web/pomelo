<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clinic_id',
    ];

}
