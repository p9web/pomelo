# Pomelo - Laravel

## Installation
1. Clone this repo;
1. Run ***"php composer install"*** command to install dependencies;
1. Create an empty ***database.sqlite*** file in the ***/database*** folder;
1. Run ***"php artisan migrate:fresh --seed"*** command to build and seed app's database;
1. Run ***"php artisan serve"*** command to launch dev server;

The api is now locally accessible at ***http://localhost:8000/api/...***. Use Postman to interact with it.


## Introduction

This application was created at the request of Logient in order to allow Pomelo to assess certain technical skills and aptitudes.

***Implement a simplified REST API that will support the fallowing features.***

### Requested features:
1. A patient can search for the different providers of the clinic
1. A patient can look for the availabilities of a specific provider within a defined time interval (for instance, the availabilities of Dr. A between May 8th, 2019 and May 12th, 2019)
1. A patient can book an appointment with a provider by selecting one of their availabilities

### Notes:
- Clinic
  - Only one clinic is to be supported
  - A clinic has only one attribute, its name, which also acts as its unique identifier (ID)
  - A clinic can have one or several patients
  - A clinic can have one or several providers
- Patient
  - A patient has the following attributes: First name, Last name, The combination of the first and last names acts as an ID
- Provider
  - A provider has the following attributes: First name, Last name, The combination of the first and last names acts as an ID
- Availability
  - An availability is a time-slot during which a provider is able to treat a patient, if the latter has booked an appointment
  - Each provider has one or several availabilities everyday
  - An availability has the following attributes: Start date and time (timestamp), End date and time (timestamp), Availabilities are 15-minutes time-slots (8:00, 8:15, 8:30, etc.)
- Appointment
  - An availability that's been chosen by a patient is converted into an appointment upon booking (one availability = one appointment)
  - An appointment has the following attributes: Patient, Provider, Start date and time (timestamp), End date and time (timestamp)
  - As appointments are booked on availabilities, they are also 15-minutes time-slots

---


## DB Structure:
- Clinics
  - name (primary key)
- Patients
  - first_name
  - last_name
  - clinic_id
  * Primary key: [first_name, last_name]
- Providers
  - first_name
  - last_name
  - clinic_id
  * Primary key: [first_name, last_name]
- Time_Slots
  - clinic_id
  - provider_id
  - patient_id
  - start_datetime
  - end_datetime
  * Unique: [provider_id, start_datetime]
  * Unique: [patient_id, start_datetime]


## Routes:
- Providers
  - [x] GET /api/providers/:clinic_id (returns providers by clinic)
- Availabilities
  - [x] GET /api/availabilities/:provider_id/:from/:to (returns unbooked time slots by provider in a specific timeframe)
- Apointments
  - [x] POST /api/appointment (turns availalibity into appointment)
  

## TODO:
- [ ] Define associations between tables
- [ ] Check if some current controllers functions should be moved to corresponding models


## COMMENTS:
This is my first Laravel experience. Thanks for your understanding.
